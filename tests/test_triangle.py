import unittest
import numpy
from waveguide import Triangle, Waveguide


class TestTriangle(unittest.TestCase):
    """Test methods in Triangle class
    """

    def test_area(self):
        # Set up triangle object
        ids = numpy.array([1, 2, 3])
        coordinates = numpy.array([[0, 0], [0, 1], [1, 0]])

        tri = Triangle(ids, coordinates)
        self.assertEquals(tri.area(), 0.5)

    def test_compute_nodal_coefficients(self):
        # Set up triangle object
        ids = numpy.array([1, 2, 3])
        coordinates = numpy.array([[0, 0], [0, 2], [3, 0]])

        tri = Triangle(ids, coordinates)
        tri.computeNodeCoefficients()

        self.assertEqual(tri.nodes[0].coefficients.tolist(
        ), numpy.array([-6, 2, 3]).tolist())
        self.assertEqual(tri.nodes[1].coefficients.tolist(
        ), numpy.array([0, 0, -3]).tolist())
        self.assertEqual(tri.nodes[2].coefficients.tolist(
        ), numpy.array([0, -2, 0]).tolist())

    def test_compute_element_s_matrix(self):
        # Set up triangle object
        ids = numpy.array([1, 2, 3])
        coordinates = numpy.array([[0, 0], [0, 2], [3, 0]])

        tri = Triangle(ids, coordinates)
        tri.computeNodeCoefficients()
        tri.computeElementSMatrix()

        expected = (1/12*numpy.array([[13, -9, -4], [-9, 9, 0], [-4, 0, 4]]))
        self.assertEqual(tri.S.tolist(), expected.tolist())

    def test_compute_element_t_matrix(self):
        # Set up triangle object
        ids = numpy.array([1, 2, 3])
        coordinates = numpy.array([[0, 0], [0, 2], [3, 0]])

        tri = Triangle(ids, coordinates)
        tri.computeElementTMatrix()

        expected = 3/12*numpy.array([[2, 1, 1], [1, 2, 1], [1, 1, 2]])
        self.assertEqual(tri.T.tolist(), expected.tolist())

    def test_global_matrix_assembly(self):
        # Make a waveguide with two triangles
        points = numpy.array([[1, 1], [0, 1], [0, 0], [0, 2]])
        cells = {}
        cells['triangle'] = numpy.array([[0, 1, 2], [1, 3, 0]])
        wg = Waveguide(points, cells)
        wg.triangles[0].S = numpy.random.rand(3, 3)
        wg.triangles[0].T = numpy.random.rand(3, 3)
        wg.triangles[1].S = numpy.random.rand(3, 3)
        wg.triangles[1].T = numpy.random.rand(3, 3)

        wg.assembleGlobalMatrix()

        S1 = wg.triangles[0].S
        S2 = wg.triangles[1].S
        self.assertEqual(wg.S[0, 0], S1[0, 0]+S2[2, 2])
        self.assertEqual(wg.S[1, 0], S1[1, 0]+S2[0, 2])
        self.assertEqual(wg.S[3, 2], 0)

    def test_compute_gradient(self):
        # Set up triangle object
        ids = numpy.array([1, 2, 3])    
        coordinates = numpy.array([[0, 0], [0, 2], [3, 0]])
        tri = Triangle(ids, coordinates)
        tri.computeNodeCoefficients()

        expected = 1/(4*3*3)*numpy.array([[2*2+3*3,2*0+3*-3,2*-2+3*0],[0*2+-3*3,0+-3*-3,0*-2+-3*0],[-2*2+0*3,-2*0+0*-3,-2*-2+0*0]])
        
        self.assertEqual(tri.computeGrad().tolist(),expected.tolist())
if __name__ == '__main__':
    unittest.main()
