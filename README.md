# Waveguide Mode Solver

[![pipeline status](https://gitlab.com/tsmithhisler/waveguide-mode-solver/badges/master/pipeline.svg)](https://gitlab.com/tsmithhisler/waveguide-mode-solver/commits/master) [![coverage report](https://gitlab.com/tsmithhisler/waveguide-mode-solver/badges/master/coverage.svg)](https://gitlab.com/tsmithhisler/waveguide-mode-solver/commits/master)

This project is meant to implement a "full-stack" application for modal analysis of arbitrarily shaped waveguides. The backend makes use of the popular python packages numpy and scipy to solve the finite-element eigenvalue problem. The methodology is presented in section 2 of <https://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/19950011772.pdf>.

Solution data (Mesh/Field vector data) will be provided to the frontend through the python package flask and transported as JSON.

The front end will utilize react components and provide visualization via plotly (<https://plot.ly/javascript/react/>).

## Requirements

Gmsh (<http://gmsh.info/>) is required to be on the path in order to create the finite element triangulation/mesh.

All other python package requirements are contained in the environment.yml and can be installed using anaconda/miniconda via:

```bash
conda env create -f environment.yml
```

## Running The Application

From one terminal session, run the following code to start the python backend:

```bash
conda activate waveguides
./demo.sh
```

From another terminal session, run the following code to start the frontend:

```bash
cd react-app
npm start
```

## TODO

- [x] Implement eigenvalue solver in python as package
- [x] Figure out front end
- [x] CSS/Styling
- [x] CI/CD testing
- [ ] CI/CD linting
- [ ] Docker container -> Gitlab registry for faster testing
- [x] Implement machine learning for determining mode number