import numpy as np
from scipy.sparse import linalg as LA


class Node:
    """Class that contains information related to the coordinates as well as the 

    Attributes:
        x {str}: 
        y {int}: 
        coefficients {numpy.ndarray}: coefficients of linear interpolation function of the form [a,b,c] -> a + bx + cy

    """

    def __init__(self, id, coordinates):
        """Constructor for Node class

        Arguments:
            id {integer} -- Global identification number of node in triangulation
            coordinates {numpy.ndarray} -- Numpy array of X/Y coordinates for node
        """

        self.id = int(id)
        self.x = coordinates[0]
        self.y = coordinates[1]
        self.coefficients = np.array([0, 0, 0])


class Triangle:
    """Class representation of triangular 2-dimensional finite element.

    Attributes:
        nodes {list} --
    """

    def __init__(self, ids, coordinates):
        """[summary]

        Arguments:
            ids {[type]} -- [description]
            coordinates {[type]} -- [description]
        """

        self.nodes = []
        for idx, val in enumerate(ids):
            self.nodes.append(Node(val, coordinates[idx, :]))

        self.computeNodeCoefficients()
        self.computeElementSMatrix()
        self.computeElementTMatrix()

    def area(self):
        """Computes the area of the given triangle

        Returns:
            np.float64 -- Area of the triangle
        """

        matrix = np.ones((3, 3))
        for idx, node in enumerate(self.nodes):
            matrix[idx, 1] = node.x
            matrix[idx, 2] = node.y
        return np.abs(0.5*np.linalg.det(matrix))

    def computeNodeCoefficients(self):
        """

        Returns:
            None
        """

        # print('Computing Nodal Coefficients..')
        # for each node in the triangle
        for idx in np.arange(3):

            # np.roll accomplishes cyclical indexing of nodes in triangle
            # First iteration  -> i=0,j=1,k=2
            # Second iteration -> i=1,j=2,k=0
            # Third iteration  -> i=2,j=0,k=1
            [_, j, k] = np.roll(np.arange(3), -idx)

            # X and Y coordinates of the jth/kth element
            x_j = self.nodes[j].x
            x_k = self.nodes[k].x
            y_j = self.nodes[j].y
            y_k = self.nodes[k].y

            # Compute ith coefficients from jth and kth x and y coordinates
            a = x_j*y_k-x_k*y_j
            b = y_j-y_k
            c = x_k-x_j

            # Assign coefficients to ith node (cyclically)
            self.nodes[idx].coefficients = np.array([a, b, c])

    def computeElementSMatrix(self):
        """[summary]

        Returns:
            None
        """

        gradi_gradj = self.computeGrad()
        self.S = self.area()*gradi_gradj

    def computeElementTMatrix(self):
        # print('Computing Element T Matrix..')
        self.T = 1/12*self.area()*np.array([[2, 1, 1], [1, 2, 1], [1, 1, 2]])

    def getGlobalNodeNumbers(self):
        """[summary]

        Returns:
            np.ndarray -- 3x1 Numpy array of global node numbers for each node in triangle
        """

        # Preallocate array to be returned
        nums = np.empty((3,))

        # For each node in node list
        for idx, node in enumerate(self.nodes):
            nums[idx] = node.id
        return nums

    def computeGrad(self):
        """[summary]

        Returns:
            np.ndarray -- 3x3 Numpy array containing the products of the ith nodal gradient and the jth nodal gradient
        """

        # Pre-allocate the [grad(i)*grad(j)] array to be returned
        gradi_gradj = np.zeros((3, 3))

        # Get each of the nodal coefficients and store in local 3x3 array
        coefficients = np.zeros((3, 3))
        for idx, node in enumerate(self.nodes):
            coefficients[idx, :] = node.coefficients

        # Pull out b and c coefficients
        b = coefficients[:, 1]
        c = coefficients[:, 2]

        # Compute products
        gradi_gradj[0, 0] = b[0]*b[0]+c[0]*c[0]
        gradi_gradj[0, 1] = b[0]*b[1]+c[0]*c[1]
        gradi_gradj[0, 2] = b[0]*b[2]+c[0]*c[2]
        gradi_gradj[1, 0] = b[1]*b[0]+c[1]*c[0]
        gradi_gradj[1, 1] = b[1]*b[1]+c[1]*c[1]
        gradi_gradj[1, 2] = b[1]*b[2]+c[1]*c[2]
        gradi_gradj[2, 0] = b[2]*b[0]+c[2]*c[0]
        gradi_gradj[2, 1] = b[2]*b[1]+c[2]*c[1]
        gradi_gradj[2, 2] = b[2]*b[2]+c[2]*c[2]

        # Return array scaled by 1/(4*A^2)
        return 1/(4*self.area()*self.area())*gradi_gradj

    def center(self):
        """Compute barycentric center of triangle element based on x/y coordinates of nodes. Usefull for visualization of electric fields.

        Returns:
            np.ndarray -- 2x1 Numpy array containing x and y center of triangle.
        """

        # Get sum of all x and y coordinates for each node
        x = 0
        y = 0
        for node in self.nodes:
            x = x+node.x
            y = y+node.y

        # Compute average x/y value
        x = x/3.0
        y = y/3.0

        return np.array([x, y])


class Waveguide:
    def __init__(self, points, cells):
        self.triangles = []
        self.total_nodes = len(np.unique(cells['triangle']))

        # Need to renumber global node numbers
        g_nodes = np.sort(np.unique(cells['triangle']))
        renumber = dict(zip(g_nodes, np.arange(self.total_nodes)))

        # For each triangle in mesh
        for i in range(cells['triangle'].shape[0]):
            nodes_in_tri = cells['triangle'][i]
            node_coordinates = points[nodes_in_tri, :]

            # Perform renumbering
            nodes_in_tri = np.array([renumber[val] for val in nodes_in_tri])

            # Append triangle to self.triangles
            self.triangles.append(Triangle(nodes_in_tri, node_coordinates))

    def assembleGlobalMatrix(self):
        print('Assembling global matrix...')

        self.S = np.zeros((self.total_nodes, self.total_nodes))
        self.T = np.zeros((self.total_nodes, self.total_nodes))
        for tri in self.triangles:
            S_el = tri.S
            T_el = tri.T
            gid = tri.getGlobalNodeNumbers().astype(int)
            GID2, GID1 = np.meshgrid(gid, gid)
            self.S[GID1, GID2] = self.S[GID1, GID2]+S_el
            self.T[GID1, GID2] = self.T[GID1, GID2]+T_el
        print('Global matrix assembly complete!')

    def solve(self):
        print('Solving linear system...')
        w, v = LA.eigs(A=self.S, k=10, M=self.T, which='SM')
        print('Linear system solved!')

        # Get positive eigenvalues only
        gt_0 = np.where(w > 1e-3)
        w = w[gt_0]
        v = np.squeeze(v[:, gt_0])

        # Sort
        sort_idx = np.argsort(w)
        w = w[sort_idx]
        v = v[:, sort_idx]

        # Assign nodal psi values back to each node in triangle
        for tri in self.triangles:
            for node in tri.nodes:
                gid = node.id
                node.psi = v[gid, :]

        self.n_modes = len(w)
        return (np.sqrt(w.real), v)

    def computeFields(self):
        fields = np.zeros((len(self.triangles), 2, self.n_modes))
        for idx, tri in enumerate(self.triangles):
            dpsi_dx = None
            for node in tri.nodes:
                if dpsi_dx is None:
                    dpsi_dx = node.psi*node.coefficients[1]
                    dpsi_dy = node.psi*node.coefficients[2]
                else:
                    dpsi_dx = dpsi_dx+node.psi*node.coefficients[1]
                    dpsi_dy = dpsi_dy+node.psi*node.coefficients[2]

            tri.Ex = -1/(2*tri.area())*dpsi_dy
            tri.Ey = 1/(2*tri.area())*dpsi_dx

            fields[idx, 0, :] = np.real(tri.Ex)
            fields[idx, 1, :] = np.real(tri.Ey)
        return fields

    def getCenters(self):
        """

        Returns:
            [type] -- [description]
        """

        centers = np.zeros((len(self.triangles), 2))
        for idx, tri in enumerate(self.triangles):
            centers[idx, :] = tri.center()
        return centers
